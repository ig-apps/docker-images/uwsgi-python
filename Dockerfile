FROM ossobv/uwsgi-python:3.10

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install gcc libpq-dev python3-dev python3-pip python3-venv python3-wheel build-essential -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN python3 -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"
ENV VIRTUAL_ENV="/opt/venv"
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV PYTHONHASHSEED=random
ENV PYTHONIOENCODING=UTF-8

RUN pip install wheel setuptools --upgrade

